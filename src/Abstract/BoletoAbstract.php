<?php

namespace Config_Boletos\Abstract;

use Config_Boletos\Exception\FinneBoletoException;
use Config_Boletos\Interface\PersonInterface;
use DateTime;
use ErrorException;

abstract class BoletoAbstract
{
    //Consts
    protected const CURRENCY_NUMBER = 9;
    protected static $currencyAvailabe = [
        self::CURRENCY_NUMBER => 'REAL',
    ];
    protected string $bankCode = '001';
    protected int $currencyNumber = self::CURRENCY_NUMBER;
    protected string $htmlLayout = 'default.phtml';
    protected string $assets = __DIR__ . '/../../assets';
    protected string $bankLogo = 'default.jpg';
    protected string $companyLogo = 'finne.png';
    //End Constants
    //Variables
    protected float|null $value = null;
    protected float|null $valueLatePayment = null;
    protected float|null $valuePayment = null;
    protected float|null $valueUnit = null;
    protected float|null $valueMinPayment = null;
    protected float|null $valueDiscount = null;
    protected float|null $valueOtherDiscounts = null;
    protected float|null $valueOtherAdditions = null;
    protected bool $valuePrint = true;
    protected bool $printInstructions = true;
    protected string $ourNumber;

    protected PersonInterface $holder;
    protected PersonInterface $drawer;
    protected PersonInterface|null $customer = null;

    protected string|null $documentNumber = null;
    protected string $numberInstallments;
    protected int|null $sequential = null;
    //Convenio
    protected string $agreement;

    protected string $description;
    protected string $paymentLocation;
    protected array $instructions = ['Pagar até a data do vencimento.'];

    protected int|null $quantity;
    protected DateTime $dateDocument;
    protected DateTime $dateEmission;
    protected DateTime $dateDue;
    protected bool $counterPresentation = false;
    protected string $accept = 'N';
    protected string $docType = 'DM';
    protected string|null $bankUsedOnly = null;

    protected int|null $agency;
    protected string|int|null $agencyDigit = null;
    protected int $account;
    protected int|null $accountDigit = null;
    protected string $portfolio;
    protected array $portfolioAvailable = [];
    protected array $portfolioName = [];
    protected string $bankName = 'Default';

    protected array $finalData = [];
    public function __construct(array $params = [])
    {
        foreach ($params as $key => $value) {
            if (method_exists($this, 'set' . $key)) {
                $this->{'set' . $key}($value);

                continue;
            }

            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }

        if (!isset($this->dateEmission)) {
            $this->setDateEmission(null);
        }

        if (!isset($this->dateDocument)) {
            $this->setDateDocument(null);
        }
    }

    /**
     * Abstract functions
     */
    //Esse méotodo permite gerar os campos de 20 a 44
    abstract protected function getFreeField(): string;
    abstract protected function setOurNumber(): void;

    /**
     * End abstract functions
     */
    public function setDateDocument(string|null $date = null): void
    {
        if (null === $date) {
            $this->dateDocument = new DateTime();
        }

        $this->dateDocument = new DateTime($date);
    }

    public function setDateEmission(string|null $date = null): void
    {
        if (null === $date) {
            $this->dateEmission = new DateTime();
        }
    }

    public function setDateDue(string|null $date = null): void
    {
        if (null === $date) {
            $this->dateDue = new DateTime();
        }

        $this->dateDue = new DateTime($date);
    }

    public function setPortfolio(string $portfolio): void
    {
        if (!in_array($portfolio, $this->portfolioAvailable)) {
            throw new FinneBoletoException('Carteira não não disponível para o Banco do Brasil');
        }

        $this->portfolio = $portfolio;

        return;
    }

    public function setValueMinPayment(float $value): void
    {
        $this->valueMinPayment = $value;
        $this->counterPresentation = true;

        return;
    }

    /**
     * Getters functions
     */
    public function getSequential(): int|null
    {
        // if (!isset($this->sequential)) {
        //     throw new FinneBoletoException('Não foi encontrado o sequencial');
        // }

        return $this->sequential;
    }

    public function getPortfolio(): string
    {
        if (!isset($this->portfolio)) {
            throw new FinneBoletoException('Carteira não foi encontrada');
        }

        return $this->portfolio;
    }

    public function getPortfolioAvailable(): array
    {
        if (!isset($this->portfolioAvailable) || empty($this->portfolioAvailable)) {
            throw new FinneBoletoException('Nenhum grupo de carteiras definido');
        }

        return $this->portfolioAvailable;
    }

    public function getPortfolioName(): string|null
    {
        if (!empty($this->portfolioName[$this->getPortfolio()])) {
            return $this->portfolioName[$this->getPortfolio()];
        }

        return (string)$this->getPortfolio();
    }

    public function getAgency(): int
    {
        if (!isset($this->agency)) {
            throw new FinneBoletoException('Agência não foi encontrada');
        }

        return $this->agency;
    }

    public function getAccount(): string
    {
        if (!isset($this->account)) {
            throw new FinneBoletoException('Conta não foi encontrada');
        }

        return $this->account;
    }

    public function getAccountDig(): string|int|null
    {
        return $this->accountDigit;
    }

    public function getAgencyDigit(): string|int|null
    {
        return $this->agencyDigit;
    }

    public function getHolder(): PersonInterface
    {
        if (!isset($this->holder)) {
            throw new FinneBoletoException('Titular não foi encontrado');
        }

        return $this->holder;
    }

    public function getDrawer(): PersonInterface
    {
        if (!isset($this->drawer)) {
            throw new FinneBoletoException('Sacado não foi encontrado');
        }

        return $this->drawer;
    }

    public function getCustomer(): PersonInterface|null
    {
        if (!isset($this->customer) || empty($this->customer)) {
            return null;
        }

        return $this->customer;
    }

    public function getBankCode(): int|string
    {
        if (!isset($this->bankCode)) {
            throw new FinneBoletoException('Código do banco não foi encontrado');
        }

        return $this->bankCode;
    }

    public function getDateDue(): DateTime
    {
        if (!isset($this->dateDue)) {
            throw new FinneBoletoException('Data de vencimento não foi encontrada');
        }

        return $this->dateDue;
    }

    public function getCounterPresentation(): bool
    {
        if (!isset($this->counterPresentation)) {
            throw new FinneBoletoException('Apresentação do contador não foi encontrada');
        }

        return $this->counterPresentation;
    }

    public function getDateDocument(): DateTime
    {
        if (!isset($this->dateDocument)) {
            throw new FinneBoletoException('Data do documento não foi encontrada');
        }

        return $this->dateDocument;
    }

    public function getAccept(): string
    {
        if (!isset($this->accept)) {
            throw new FinneBoletoException('Aceite não foi encontrado');
        }

        return $this->accept;
    }

    public function getDocType(): string
    {
        if (!isset($this->docType)) {
            throw new FinneBoletoException('Tipo de documento não foi encontrado');
        }

        return $this->docType;
    }

    public function getDocumentNumber(): string|null
    {
        return $this->documentNumber;
    }

    public function getBankUsedOnly(): string|null
    {
        return $this->bankUsedOnly;
    }

    public function getDateEmission(): DateTime
    {
        if (!isset($this->dateEmission)) {
            throw new FinneBoletoException('Data de emissão não foi encontrada');
        }

        return $this->dateEmission;
    }

    public function getInstructions(): array
    {
        if (!isset($this->instructions)) {
            throw new FinneBoletoException('Instruções não foram encontradas');
        }

        return $this->instructions;
    }

    public function getPaymentLocation(): string
    {
        if (!isset($this->paymentLocation)) {
            throw new FinneBoletoException('Local de pagamento não foi encontrado');
        }

        return $this->paymentLocation;
    }

    public function getCurrencyNumber(): int
    {
        if (!isset($this->currencyNumber)) {
            throw new FinneBoletoException('Moeda não foi encontrada');
        }

        return $this->currencyNumber;
    }

    public function getValue(): float|null
    {
        return $this->getCounterPresentation() ? 0.00 : $this->value;
    }

    public function getValueDiscount(): float|null
    {
        return $this->valueDiscount;
    }

    public function getValueLatePayment(): float|null
    {
        return $this->valueLatePayment;
    }

    public function getValueOtherDiscounts(): float|null
    {
        return $this->valueOtherDiscounts;
    }

    public function getValueOtherAdditions(): float|null
    {
        return $this->valueOtherAdditions;
    }

    public function getQuantity(): int|null
    {
        if (!isset($this->quantity)) {
            return null;
        }

        return $this->quantity;
    }

    public function getValuePayment(): float|null
    {
        return $this->valuePayment;
    }

    public function getValueUnit(): float|null
    {
        return $this->valueUnit;
    }

    public function getValueMinPayment(): float|null
    {
        return $this->valueMinPayment;
    }

    public function getHtmlLayout(): string
    {
        return $this->htmlLayout;
    }

    public function getAssets(): string
    {
        return $this->assets;
    }

    public function getPrintInstructions(): bool
    {
        return $this->printInstructions;
    }

    public function getBankLogo(): string
    {
        return $this->bankLogo;
    }

    public function getBankLogoBase64(): string
    {
        $logoName = $this->getBankLogo();
        $pathInfo = pathinfo($logoName, PATHINFO_EXTENSION);
        $assetsDir = $this->getAssets();

        $base64 = 'data:image/' . $pathInfo . ';base64,' .
         base64_encode(file_get_contents($assetsDir . '/img/bank_logos/' . $logoName));

        return $base64;
    }

    public function getCompanyLogo(): string
    {
        return $this->companyLogo;
    }

    public function getCompanyLogoBase64(): string
    {
        $logoName = $this->getCompanyLogo();
        $pathInfo = pathinfo($logoName, PATHINFO_EXTENSION);
        $assetsDir = $this->getAssets();

        $base64 = 'data:image/' . $pathInfo . ';base64,' .
         base64_encode(file_get_contents($assetsDir . '/img/' . $logoName));

        return $base64;
    }

    public function getOurNumber($formatted = true): string
    {
        $number = $this->ourNumber;

        if (!$formatted) {
            $number = str_replace(['.', '/', ' ', '-'], '', $number);
        }

        return $number;
    }

    public function getValuePrint(): bool
    {
        return $this->valuePrint;
    }

    public function getAgencyHolderCode(): string
    {
        $agency = empty($this->getAgencyDigit()) ?
            $this->getAgency() : $this->getAgency() . '-' . $this->getAgencyDigit();
        $account = empty($this->getAccountDig()) ?
            $this->getAccount() : $this->getAccount() . '-' . $this->getAccountDig();

        return $agency . ' / ' . $account;
    }

    public function getBarCodeImage(): string
    {
        $febrabanNumber = $this->getFebrabanNumber();
        $barcode = ['00110', '10001', '01001', '11000', '00101', '10100', '01100', '00011', '10010', '01010'];

        for ($forOne = 9; $forOne >= 0; $forOne--) {
            for ($forTwo = 9; $forTwo >= 0; $forTwo--) {
                $for = ($forOne * 10) + $forTwo;
                $string = '';

                for ($temp = 1; $temp < 6; $temp++) {
                    $string .= substr($barcode[$forOne], ($temp - 1), 1);
                    $string .= substr($barcode[$forTwo], ($temp - 1), 1);
                }
                $barcode[$for] = $string;
            }
        }

        //String base
        $final = '<div class="barcode">' .
                '<div class="black thin"></div>' .
                '<div class="white thin"></div>' .
                '<div class="black thin"></div>' .
                '<div class="white thin"></div>';

        if (strlen($febrabanNumber) % 2 != 0) {
            $febrabanNumber = '0' . $febrabanNumber;
        }

        $position = 0;
        foreach (str_split($febrabanNumber, 2) as $currentNumber) {
            $temp = (int) round((float) self::leftChar($currentNumber, 2));
            $reference = $barcode[$temp];

            for ($i = 1; $i < 11; $i += 2) {
                $forOne = substr($reference, ($i - 1), 1) == 0 ? 'thin' : 'large';
                $forTwo = substr($reference, $i, 1) == 0 ? 'thin' : 'large';
                $final .= '<div class="black ' . $forOne . '"></div>';
                $final .= '<div class="white ' . $forTwo . '"></div>';
            }

            $position += 2;
        }

        $final .= '<div class="black large"></div>' .
                    '<div class="white thin"></div>' .
                    '<div class="black thin"></div>' .
                    '</div>';

        return $final;
    }

    public function getBankName(): string
    {
        return $this->bankName;
    }


    public function getOutput(): string
    {
        ob_start();

        $dateDue = $this->getCounterPresentation() ? 'Contra apresentação' : $this->getDateDue()->format('d/m/Y');
        $this->finalData = [
            'writable_line' => $this->getWritableLine(),
            'holder' => $this->getHolder()->getName(),
            'holder_document' => $this->getHolder()->getDocumentId(),
            'holder_address_1' => $this->getHolder()->getAddress(),
            'holder_address_2' => $this->getHolder()->getFormattedAddress(),
            'bank_logo' => $this->getBankLogoBase64(),
            'bank_name' => $this->getBankName(),
            'company_logo' => $this->getCompanyLogoBase64(),
            'bank_code_with_digit' => $this->getBankCodeWithDigit(),
            'currency' => static::$currencyAvailabe[$this->getCurrencyNumber()],
            'quantity' => $this->getQuantity(),
            'date_due' => $dateDue,
            'date_emission' => $this->getDateEmission()->format('d/m/Y'),
            'date_document' => $this->getDateDocument()->format('d/m/Y'),
            'value_min_payment' => $this->getValueMinPayment(),
            'value' => number_format($this->getValue(), 2, ',', '.'),
            'value_discount' => $this->getValueDiscount(),
            'value_late_payment' => $this->getValueLatePayment(),
            'value_other_discounts' => $this->getValueOtherDiscounts(),
            'value_other_additions' => $this->getValueOtherAdditions(),
            'value_payment' => $this->getValuePayment(),
            'value_unit' => $this->getValueUnit(),
            'value_print' => $this->getValuePrint(),
            'value_print_format' => $this->getValuePrint() ? 'R$ ' . number_format($this->getValue(), 2, ',', '.') : '',
            'customer' => !empty($this->getCustomer()) ? $this->getCustomer()->getNameAndDocument() : null,
            'customer_address_1' => !empty($this->getCustomer()) ? $this->getCustomer()->getAddress() : null,
            'customer_address_2' => !empty($this->getCustomer()) ? $this->getCustomer()->getFormattedAddress() : null,
            'drawer' => $this->getDrawer()->getName(),
            'drawer_document' => $this->getDrawer()->getDocumentId(),
            'drawer_address_1' => $this->getDrawer()->getAddress(),
            'drawer_address_2' => $this->getDrawer()->getFormattedAddress(),
            'description' => [] + [null, null . null, null, null],
            'instructions' => (array) $this->getInstructions() + [null, null, null, null, null, null, null, null],
            'payment_location' => $this->getPaymentLocation(),
            'document_number' => $this->getDocumentNumber(),
            'agency_holder' => $this->getAgencyHolderCode(),
            'our_number' => $this->getOurNumber(),
            'doc_type' => $this->getDocType(),
            'accept' => $this->getAccept(),
            'portfolio_name' => $this->getPortfolioName(),
            'bank_use_only' => $this->getBankUsedOnly(),
            'barcode' => $this->getBarCodeImage(),
            'assets' => $this->getAssets(),
            'febraban_number' => $this->getFebrabanNumber(),
            'print_instructions' => $this->getPrintInstructions(),
            'print_value' => $this->getValuePrint(),
        ];

        extract($this->finalData);

        @include $this->getAssets() . '/phtml/' . $this->getHtmlLayout();

        return ob_get_clean();
    }

    public function getHtmlHeader(): string
    {
        try {
            ob_clean();
        } catch (ErrorException $e) {
            ob_start();
            ob_clean();
        }

        $assets = $this->getAssets();
        $bankName = $this->getBankName();
        @include $this->getAssets() . '/phtml/components/header.phtml';

        return ob_get_clean();
    }

    public function getHtmlFooter(): string
    {
        try {
            ob_clean();
        } catch (ErrorException $e) {
            ob_start();
            ob_clean();
        }

        @include $this->getAssets() . '/phtml/components/footer.phtml';

        return ob_get_clean();
    }

    public function getPageBreak(): string
    {
        try {
            ob_clean();
        } catch (ErrorException $e) {
            ob_start();
            ob_clean();
        }


        @include $this->getAssets() . '/phtml/components/page-break.phtml';

        return ob_get_clean();
    }

    /**
     * End Getters functions
     */


    //Aux functions
    protected static function zeroFill(string $value, int $lenght)
    {
        $value = (string)$value;

        if (strlen($value) > $lenght) {
            throw new FinneBoletoException('O tamanho do campo é menor do que o valor informado');
        }

        return str_pad($value, $lenght, '0', STR_PAD_LEFT);
    }

    /**
     * Calcula e retorna o dígito verificador usando o algoritmo Modulo 10
     * @param non-empty-string $num
     * @return int
     */
    protected static function module10(int $number)
    {
        $totalNumberModule10 = 0;
        $factorRate = 2;

        for ($i = strlen($number); $i > 0; $i--) {
            $numbers[$i] = (int) substr($number, $i - 1, 1);

            $temp = $numbers[$i] * $factorRate;
            $tempZero = 0;
            foreach (preg_split('//', (string) $temp, -1, PREG_SPLIT_NO_EMPTY) as $value) {
                $tempZero += $value;
            }

            $partialTen[$i] = $tempZero;
            $totalNumberModule10 += $partialTen[$i];

            if ($factorRate == 2) {
                $factorRate = 1;
            } else {
                $factorRate = 2;
            }
        }

        $rest = $totalNumberModule10 % 10;
        $digit = 10 - $rest;

        $digit = ($digit == 10) ? 0 : $digit;

        return $digit;
    }

    /**
     * Calcula e retorna o dígito verificador usando o algoritmo Modulo 11
     *
     * @param non-empty-string $num
     * @param int $base
     * @return array ['digit' => int, 'rest' => int] - Retorna um array com as chaves 'digito' e 'resto'
     */
    protected static function module11(int|string $number, $base = 9, $t = false)
    {
        $factorRate = 2;
        $sum = 0;
        $partial = [];

        for ($i = strlen($number); $i > 0; $i--) {
            //Get Number
            $numbers[$i] = (int) substr($number, $i - 1, 1);
            $partial[$i] = $numbers[$i] * $factorRate;
            $sum += $partial[$i];

            if ($t) {
                var_dump(
                    [
                        'numero inicial:' => $number,
                        'numero convertido:' => $numbers[$i],
                        'fator' => $factorRate,
                        'soma' => $sum,
                        'parcial' => $partial[$i],
                    ]
                );
            }

            if ($factorRate == $base) {
                $factorRate = 1;
            }

            $factorRate++;
        }

        $result = [
            'digit' => ($sum * 10) % 11,
            'rest' => $sum % 11,
        ];

        if ($result['digit'] == 10) {
            $result['digit'] = 0;
        }

        return $result;
    }

    protected function getWritableLine(bool $formatted = true): string
    {
        $key = $this->getFreeField();

        //Essa função vai quebrar as 20 posições para 44, em blocos de 3 e 5, de 10 em 10
        $configs = [
            '20-24' => substr($key, 0, 5),
            '25-34' => substr($key, 5, 10),
            '35-44' => substr($key, 15, 10),
        ];

        //Concatenar codigo do bank, codigo da moeda e o primo bloco de 5 caracteres para identificar o digito
        //Verificador da parte 1
        $checkDigit = static::module10($this->getBankCode() . $this->getCurrencyNumber() . $configs['20-24']);

        //Mudar um pontos no bloco 20-24  na segunda posição
        $configs['20-24'] = substr_replace($configs['20-24'], '.', 1, 0);

        //Construindo a primeira parte
        $firstPart = $this->getBankCode() . $this->getCurrencyNumber() . $configs['20-24'] . $checkDigit;

        //Calcular o digito verificador da segunda parte
        $checkDigit2 = static::module10($configs['25-34']);
        $secondPart = $configs['25-34'] . $checkDigit2;
        $secondPart = substr_replace($secondPart, '.', 5, 0);

        //Calcular o digito verificador da terceira parte
        $checkDigit3 = static::module10((int)$configs['35-44']);
        $thirdPart = $configs['35-44'] . $checkDigit3;
        $thirdPart = substr_replace($thirdPart, '.', 5, 0);

        $fourthPart = $this->getHumanReadable();

        $fifthPart = $this->getDueFactor() . $this->getValueZeroFill();

        $writableLine = "$firstPart  $secondPart  $thirdPart  $fourthPart  $fifthPart";
        if (!$formatted) {
            return preg_replace('/[^0-9]/', '', $writableLine);
        }

        return $writableLine;
    }

    /**
     * Função para calcular o digito verificador do nosso número. Ou Dígito Verificador
     *
     * @return int
     */
    protected function getHumanReadable(): int
    {
        $number = self::zeroFill($this->getBankCode(), 4);
        $number .= $this->getCurrencyNumber() . $this->getDueFactor();
        $number .= $this->getValueZeroFill() . $this->getFreeField();

        $module = static::module11($number);

        $readable = 11 - $module['rest'];
        if ($module['rest'] == 0 || $module['rest'] == 1 || $module['rest'] == 10) {
            $readable = 1;
        }

        return $readable;
    }

    /**
     * Função para calcular o numero de dias a partir de 07/10/1997 até a data de vencimento.
     * Caso a data seja maior que 21/02/2025, usará a data 21/02/2025.
     * Caso não exista uma data, será usado 000. (contra apresentação)
     *
     * @return string
     */
    protected function getDueFactor(): string
    {
        if ($this->getCounterPresentation()) {
            return '0000';
        }

        $dateDue = $this->getDateDue();
        $referenceDate = new DateTime('2025-02-22');
        $initalDate = new DateTime('1997-10-07');

        if ($dateDue > $referenceDate) {
            return (string) $referenceDate->diff($dateDue)->days + '1000';
        }

        return (string) $initalDate->diff($dateDue)->days;
    }

    /**
     * Funcão para pegar o valor do boleto com 10 digitos e a remoção dos caracteres especiais
     *
     * @return string
     */
    public function getValueZeroFill(): string
    {
        $number = number_format($this->getValue(), 2, '', '');

        return str_pad($number, 10, '0', STR_PAD_LEFT);
    }

    public function getBankCodeWithDigit(): string
    {
        $bankCode = $this->getBankCode();
        $digit = static::module11($bankCode);

        return $bankCode . '-' . $digit['digit'];
    }

    public function getFebrabanNumber(): string
    {
        $febraban = self::zeroFill($this->getBankCode(), 3) . $this->getCurrencyNumber() . $this->getHumanReadable();
        $febraban .= $this->getDueFactor() . $this->getValueZeroFill() . $this->getFreeField();

        return $febraban;
    }

    public function leftChar(string $string, $size): string
    {
        return substr($string, 0, $size);
    }
}
