<?php

namespace Boletos;

use Config_Boletos\Exception\FinneBoletoException;
use Config_Boletos\Interface\BoletoInterface;

class Boleto
{
    protected static $banks = [
        1 => 'Banco Do Brasil',
    ];

    public static function getByBankCode(int $bankCode, array $params = []): BoletoInterface
    {
        if (!isset(self::$banks[$bankCode])) {
            $message = 'O banco de código %s não é suportado.';

            throw new FinneBoletoException(sprintf($message, $bankCode));
        }
        $bank = self::$banks[$bankCode];

        return self::getByBankName($bank, $params);
    }

    public static function getByBankName(string $bankName, array $params = []): BoletoInterface
    {
        $bankName = ucwords(strtolower($bankName));
        $clean = str_replace(' ', '', $bankName);
        $namespace = "Config_Boletos\Product\\" . $clean;

        if (!class_exists($namespace)) {
            $message = 'O arquivo de boleto do banco %s não foi encontrado.';

            throw new FinneBoletoException(sprintf($message, $bankName));
        }

        /** @var BoletoInterface $product */
        $product = new $namespace($params);

        return $product;
    }
}
