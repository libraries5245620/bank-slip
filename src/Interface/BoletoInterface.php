<?php

namespace Config_Boletos\Interface;

interface BoletoInterface
{
    public function getHtmlHeader(): string;
    public function getOutput(): string;
    public function getHtmlFooter(): string;
    public function getPageBreak(): string;
}
