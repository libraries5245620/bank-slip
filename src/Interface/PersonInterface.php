<?php

namespace Config_Boletos\Interface;

interface PersonInterface
{
    public function __construct(
        string $name,
        string $documentId,
        string $address,
        string $zipCode,
        string $state,
        string $city
    );
    public function getName();
    public function getDocumentId();
    public function getAddress();
    public function getZipCode();
    public function getState();
    public function getCity();
    public function getNameAndDocument();
    public function getDocumentType();
    public function getFormattedAddress();
}
