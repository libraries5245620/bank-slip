<?php

namespace Config_Boletos\Examples;

use Boletos\Drawer;
use Boletos\Holder;

final class BancoDoBrasilExamples
{
    private static function createMock(): array
    {
        $holder = new Holder('FACILICRED SCM LTDA', '04849745000180', 'Rua Doutor Custódio Junqueira', '', 'MG', 'Leopoldina');
        $drawer = new Drawer('ABC ATACACADO BRASILEIRO DA CONSTRUCAO SA', '38542718004684', 'RUA JOANA D ARC - ANTA CRUZ', '36088390', 'MG', 'Juiz de Fora');

        return [
            'dateDue' => '2024-05-23',
            'dateDocument' => '2024-05-07',
            'datePayment' => '2024-05-07',
            'documentNumber' => '12079',
            'quantity' => 1,
            'value' => 4110.00,
            // 'sequential' => 778358,
            'holder' => $holder,
            'drawer' => $drawer,
            'agency' => 471,
            'agencyDigit' => 5,
            'account' => 39013,
            'accountDigit' => 5,
            'portfolio' => '17',
            'agreement' => 3553013,
            'accept' => 'N',
            'instructions' => [ // Até 8
                'APÓS O VENCIMENTO COBRAR:',
                'MORA DIA/COM.PERMANENC R$ 0,00 ( 0,00% )',
                'MULTA DE R$ 0,00 ( 0,00% )',
            ],
            'ourNumber' => '35530130000824173',
        ];
    }

    public static function getMock(): array
    {
        $array = self::createMock();

        return $array;
    }
}
